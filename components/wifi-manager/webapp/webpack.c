// Automatically generated. Do not edit manually!.
#include <inttypes.h>
extern const uint8_t _favicon_32x32_png_start[] asm("_binary_favicon_32x32_png_start");
extern const uint8_t _favicon_32x32_png_end[] asm("_binary_favicon_32x32_png_end");
extern const uint8_t _index_html_gz_start[] asm("_binary_index_html_gz_start");
extern const uint8_t _index_html_gz_end[] asm("_binary_index_html_gz_end");
extern const uint8_t _index_e43f70_bundle_js_gz_start[] asm("_binary_index_e43f70_bundle_js_gz_start");
extern const uint8_t _index_e43f70_bundle_js_gz_end[] asm("_binary_index_e43f70_bundle_js_gz_end");
extern const uint8_t _node_modules_e43f70_bundle_js_gz_start[] asm("_binary_node_modules_e43f70_bundle_js_gz_start");
extern const uint8_t _node_modules_e43f70_bundle_js_gz_end[] asm("_binary_node_modules_e43f70_bundle_js_gz_end");
extern const uint8_t _runtime_e43f70_bundle_js_gz_start[] asm("_binary_runtime_e43f70_bundle_js_gz_start");
extern const uint8_t _runtime_e43f70_bundle_js_gz_end[] asm("_binary_runtime_e43f70_bundle_js_gz_end");
const char * resource_lookups[] = {
	"/dist/favicon-32x32.png",
	"/dist/index.html.gz",
	"/js/index.e43f70.bundle.js.gz",
	"/js/node-modules.e43f70.bundle.js.gz",
	"/js/runtime.e43f70.bundle.js.gz",
""
};
const uint8_t * resource_map_start[] = {
	_favicon_32x32_png_start,
	_index_html_gz_start,
	_index_e43f70_bundle_js_gz_start,
	_node_modules_e43f70_bundle_js_gz_start,
	_runtime_e43f70_bundle_js_gz_start
};
const uint8_t * resource_map_end[] = {
	_favicon_32x32_png_end,
	_index_html_gz_end,
	_index_e43f70_bundle_js_gz_end,
	_node_modules_e43f70_bundle_js_gz_end,
	_runtime_e43f70_bundle_js_gz_end
};
